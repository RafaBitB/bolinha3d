﻿using UnityEngine;
using System.Collections;

public class MoveCamera : MonoBehaviour {

    public GameObject objetoBola;
    private Vector3 posicaoInicial;

    // Use this for initialization
    void Start () {

        posicaoInicial = this.transform.position - objetoBola.transform.position;

	}
	
	// Update is called once per frame
	void Update () {

        transform.position = objetoBola.transform.position + posicaoInicial;

	}
}
