﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI; //Biblioteca necessária para usar os elementos de UI da Unity.

public class MovimentaBolinha : MonoBehaviour {

    private Rigidbody rb;
    public float velocidade;
    public GameObject particulaItem;
    public Text score;
    public Text gameOver;
    private int pontos;

	// Use this for initialization
	void Start () {

        rb = GetComponent<Rigidbody>();
        velocidade = 5;
        gameOver.text = "";
        score.text = score.text + pontos.ToString();

    }
    //Método invocado antes do Update
	void FixedUpdate()
    {
        if (Input.GetKeyDown(KeyCode.Space)){
            //Descobrir como parar a bolinha ao clicar em espaço
        }
        //Cria um vetor 3D onde a posição X e Z são alteradas conforme o as cetas do teclado.
        Vector3 move = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        //Adiciona uma força ao RigidBody
        rb.AddForce(move * velocidade);

    }
	// Update is called once per frame
	void Update () {

        

    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("item"))
        {
            //Instancia a particula no local onde se encontra o objeto.
            Instantiate(particulaItem, other.gameObject.transform.position, Quaternion.identity);
            Destroy(other.gameObject);
            MarcaPonto();
        }
    }

    void MarcaPonto()
    {
        pontos += 10;
        score.text = "SCORE: " + pontos.ToString();
        if (pontos == 60)
        {
            gameOver.text = "Você Venceu!";
        }
    }
}
