﻿using UnityEngine;
using System.Collections;

public class DestroiObjeto : MonoBehaviour {

	// Use this for initialization
	void Start () {
        //Invoca uma função depois de determinado tempo(1.5 segundos)
        Invoke("ApagaObjeto", 1.5f);

    }
	
	// Update is called once per frame
	void Update () {
        


	}

    void ApagaObjeto()
    {
        Destroy(this.gameObject);
    }
}
